import random
import numpy as np

# procedural database generation test

# ZONE TYPES
# zone 1 - residential level 1 (upper class)
# zone 2 - residential level 2 (middle class)
# zone 3 - residential level 3 (lower class)
# zone 4 - work 1 (university)
# zone 5 - work 2 (company)
# zone 6 - leisure

# database columns
# id, zoneid, workid, leisureid

# every person in the database either works, goes to univesity or is unemployed
# every person has a certain leisure activity

zoneTypes = {'res1' : 1, 'res2' : 2, 'res3' : 3, 'work1' : 4, 'work2' : 5, 'lei' : 6}

possibleYears = [2015]
possibleMonths = [3, 4, 5]
possibleDays = [i for i in range(1,32)]
possibleHours = [i for i in range(24)]
possibleMinutes = [i for i in range(60)]
possibleSeconds = possibleMinutes

# do this but for each zone
possibleLongitude = [3, 4, 5]
possibleLatitude = [i for i in range(37,51)]

class DateStruct :
    def __init__(self, year, month, day) :
        year = year
        month = month
        day = day

class TimeStruct :
    def __init__(self, hour, minute, second) :
        hour = hour
        minute = minute
        second = second

class DataSet :
    def __init__(self, dataID, date : DateStruct, time : TimeStruct, posx, posy) :
        dataID = dataID
        date = date 
        time = time
        posx = posx
        posy = posy

def databaseGeneration() :
    output = open("database.txt", "a")
    sum = 0
    while sum<100000 :
        randID = random.randint(10000000) # do we continue using int for IDs or do we change?
        nbLinesHome = int(np.random.normal(1000, 200, 1).round())
        nbLinesWork = int(np.random.normal(1000, 200, 1).round())
        nbLinesLeisure = int(np.random.normal(500, 100, 1).round())
        sum=sum+nbLinesHome+nbLinesWork+nbLinesLeisure

        for i in range(nbLinesHome+1) :
            year = random.choice(possibleYears)
            month = random.choice(possibleMonths)
            day = random.choice(possibleDays)
            newData = DataSet(randID)

    output.write("Now the file has more content!")
    output.close()

print(int(np.random.normal(500,100,1).round()))