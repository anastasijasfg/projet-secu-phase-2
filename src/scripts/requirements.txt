numpy==1.20.3
pandas==1.2.4
pika==1.2.0
psutil==5.8.0
python-dateutil==2.8.1
pytz==2021.1
six==1.16.0
